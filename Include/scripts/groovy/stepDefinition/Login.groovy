package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.openqa.selenium.ElementNotSelectableException

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import ch.qos.logback.core.joran.conditional.ElseAction
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovy.json.StringEscapeUtils
import internal.GlobalVariable

public class Login {
	@Given("User go to login page")
	public void user_go_to_login_page() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
		WebUI.navigateToUrl('http://103.180.125.58:8080/#/login')
		WebUI.verifyElementVisible(findTestObject('LoginPage/txt_Aplikasi_HR_SV_Jakarta'))
		WebUI.verifyElementVisible(findTestObject('LoginPage/username'))
		WebUI.verifyElementVisible(findTestObject('LoginPage/username'))
	}

	@When("User click Login button")
	public void user_click_Login_button() {
		WebUI.click(findTestObject('LoginPage/button_login'))
	}

	@Then("User (.*) login")
	public void user_login(String result) {
		if(result=='successfully') {
			WebUI.verifyElementVisible(findTestObject('DashboardPage/txt_History_Logbook'))
			WebUI.verifyElementVisible(findTestObject('DashboardPage/btn_Logbook'))
		}else if(result=='failed') {
			WebUI.verifyElementVisible(findTestObject('LoginPage/alert_UsernamePasswordInvalid'))
			WebUI.verifyElementVisible(findTestObject('LoginPage/username'))
		}
	}

	@When("User input username field with (.*) username")
	public void user_input_username_field_with_username(String username){
		WebUI.setText(findTestObject('LoginPage/username'), 'TestAccountKaryawan1')
	}

	@When("User input password field with (.*) password")
	public void user_input_password_field_with_password(String password) {
		if(password=='valid') {
			WebUI.setText(findTestObject('LoginPage/password'), 'P@sswordK1')
		}else if(password=='wrong') {
			WebUI.setText(findTestObject('LoginPage/password'), '123456789')
		}
	}

	@When("User input username field with {string}")
	public void user_input_username_field_with(String username) {
		WebUI.setText(findTestObject('LoginPage/username'), username)
	}
	@When("User input password field with {string}")
	public void user_input_password_field_with(String password) {
		WebUI.setText(findTestObject('LoginPage/password'), password)
	}
}
