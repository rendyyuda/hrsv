package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.openqa.selenium.ElementNotSelectableException

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import ch.qos.logback.core.joran.conditional.ElseAction
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovy.json.StringEscapeUtils
import internal.GlobalVariable


public class CreateLembur {

	@When("User click cuti lembur menu button")
	public void user_click_cuti_lembur_menu_button() {
		WebUI.click(findTestObject('DashboardPage/btn_Cuti_Lembur'))
	}

	@When("User click pengajuan lembur")
	public void user_click_pengajuan_lembur() {
		WebUI.click(findTestObject('DashboardPage/btn_lembur'))
	}

	@When("User click ajukan lembur")
	public void user_click_ajukan_lembur() {
		WebUI.click(findTestObject('LemburPage/btn_ajukan_lembur'))
	}

	@When("User (.*) tanggal lembur")
	public void user_tanggal_lembur(String tgl_lembur) {
		if(tgl_lembur=='select') {
			WebUI.setText(findTestObject('LemburPage/tgl_lembur'), '01052024')
		}else if(tgl_lembur=='not select') {
			WebUI.setText(findTestObject('LemburPage/tgl_lembur'), '')
		}
	}

	@When("User (.*) jam mulai lembur")
	public void user_jam_mulai_lembur(String jam_mulai) {
		if(jam_mulai=='select') {
			WebUI.click(findTestObject('LemburPage/jam_mulai'))
			WebUI.click(findTestObject('LemburPage/6_PM'))
		}else if(jam_mulai=='not select') {
			WebUI.verifyElementVisible(findTestObject('LemburPage/jam_mulai'))
		}
	}

	@When("User (.*) jam selesai lembur")
	public void user_jam_selesai_lembur(String jam_selesai) {
		if(jam_selesai=='select') {
			WebUI.click(findTestObject('LemburPage/jam_selesai'))
			WebUI.click(findTestObject('LemburPage/9_PM'))
		}else if(jam_selesai=='not select') {
			WebUI.verifyElementVisible(findTestObject('LemburPage/jam_selesai'))
		}
	}

	@When("User (.*) jenis lembur")
	public void user_jenis_lembur(String jenis_lembur) {
		if(jenis_lembur=='select') {
			WebUI.selectOptionByValue(findTestObject('LemburPage/jenis_lembur'), 'Hari Kerja', true)
		}else if (jenis_lembur=='not select') {
			WebUI.verifyElementVisible(findTestObject('LemburPage/jenis_lembur'))
		}
	}

	@When("User {string} nama pic")
	public void user_nama_pic(String nama_pic) {
		WebUI.selectOptionByValue(findTestObject('LemburPage/select_pic'), 'ff3a2e6c-6012-4b15-95a2-be96d7ebbd45', true)
	}

	@When("User not select nama pic")
	public void user_not_select_pic() {
		WebUI.verifyElementVisible(findTestObject('LemburPage/select_pic'))
	}

	@When("User (.*) keterangan lembur")
	public void user_keterangan_lembur(String ket_lembur) {
		if (ket_lembur=='input') {
			WebUI.setText(findTestObject('LemburPage/ket_lembur'), 'Create Laporan Bulanan')
		}else if (ket_lembur=='not input') {
			WebUI.setText(findTestObject('LemburPage/ket_lembur'), '')
		}
	}

	@When("User (.*) digital signature")
	public void user_digital_signature(String signature) {
		if (signature=='create') {
			WebUI.click(findTestObject('LemburPage/ttd_digital'))
		}else if (signature=='not create') {
			WebUI.verifyElementVisible(findTestObject('LemburPage/ttd_digital'))
		}
	}

	@When("User click submit")
	public void user_click_submit() {
		WebUI.click(findTestObject('LemburPage/btn_submit'))
	}

	@Then("User (.*) create lembur")
	public void user_create_lembur(String result) {
		if (result=='successfully') {
			WebUI.verifyTextPresent('Pengajuan lembur berhasil dibuat', false)
		}else if (result=='failed') {
			WebUI.verifyTextPresent('Tanggal Lembur tidak boleh kosong!', false)
		}
	}
}
