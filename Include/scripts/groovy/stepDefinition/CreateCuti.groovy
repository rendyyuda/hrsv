package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.openqa.selenium.ElementNotSelectableException

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import ch.qos.logback.core.joran.conditional.ElseAction
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovy.json.StringEscapeUtils
import internal.GlobalVariable

public class Create_Cuti {

	@When("User click cuti lembur menu")
	public void user_click_cuti_lembur_menu() {
		WebUI.click(findTestObject('DashboardPage/btn_Cuti_Lembur'))
	}

	@When("User click pengajuan cuti")
	public void user_click_pengajuan_cuti() {
		WebUI.click(findTestObject('DashboardPage/btn_cuti'))
	}

	@When("User click ajukan cuti")
	public void user_click_ajukan_cuti() {
		WebUI.click(findTestObject('CutiLemburPage/btn_ajukan_cuti'))
	}

	@When("User select date")
	public void user_select_date() {
		WebUI.click(findTestObject('CutiLemburPage/tgl_cuti'))
		WebUI.click(findTestObject('CutiLemburPage/tgl_10jan'))
		WebUI.click(findTestObject('CutiLemburPage/tgl_12jan'))
	}

	@When("User (.*) jenis cuti")
	public void user_jenis_cuti(String cuti) {
		if (cuti=='select') {
			WebUI.selectOptionByValue(findTestObject('CutiLemburPage/jenis_cuti'), 'Cuti Tahunan', true)
		}else if (cuti=='not select') {
			WebUI.verifyElementVisible(findTestObject('CutiLemburPage/jenis_cuti'))
		}
	}

	@When("User select nama pic")
	public void user_select_nama_pic() {
		WebUI.selectOptionByValue(findTestObject('CutiLemburPage/nama_pic'), 'c64be11f-ca8e-45fd-8b91-f673abc79236', true)
	}

	@When("User input keterangan cuti")
	public void user_input_keterangan_cuti() {
		WebUI.setText(findTestObject('CutiLemburPage/txt_ketcuti'), 'Holiday')
	}

	@When("User create signature")
	public void user_create_signature() {
		WebUI.click(findTestObject('CutiLemburPage/up_ttd_digital'))
	}

	@When("User click submit btn")
	public void user_click_submit_btn() {
		WebUI.click(findTestObject('CutiLemburPage/btn_submit'))
	}

	@Then("User (.*) create cuti")
	public void user_create_cuti(String result) {
		if (result=='successfully') {
			WebUI.verifyElementVisible(findTestObject('CutiLemburPage/alert_cuti_berhasil'))
		}else if (result=='failed') {
			WebUI.verifyElementVisible(findTestObject('CutiLemburPage/alert_cuti_kosong'))
		}
	}
}
