package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.openqa.selenium.ElementNotSelectableException

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import ch.qos.logback.core.joran.conditional.ElseAction
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovy.json.StringEscapeUtils
import internal.GlobalVariable


public class AddLogbook {

	@When("User click tambah logbook button")
	public void user_click_tambah_logbook_button() {
		WebUI.click(findTestObject('DashboardPage/btn_tambah_logbook'))
	}

	@When("User (.*) tanggal")
	public void user_tanggal(String tanggal) {
		if(tanggal=='select') {
			WebUI.setText(findTestObject('LogbookPage/input_tanggal'), '04012024')
		}else if(tanggal=='not select') {
			WebUI.setText(findTestObject('LogbookPage/input_tanggal'), '')
		}
	}

	@When("User (.*) jam mulai")
	public void user_jam_mulai(String jam_mulai) {
		if(jam_mulai=='select') {
			WebUI.click(findTestObject('LogbookPage/jam_mulai'))
			WebUI.click(findTestObject('LogbookPage/1_PM'))
		}else if(jam_mulai=='not select') {
			WebUI.verifyElementVisible(findTestObject('LogbookPage/jam_mulai'))
		}
	}

	@When("User (.*) jam selesai")
	public void user_jam_selesai(String jam_selesai) {
		if(jam_selesai=='select') {
			WebUI.click(findTestObject('LogbookPage/jam_selesai'))
			WebUI.click(findTestObject('LogbookPage/3_PM'))
		}else if(jam_selesai=='not select') {
			WebUI.verifyElementVisible(findTestObject('LogbookPage/jam_selesai'))
		}
	}
	@When("User (.*) mode kerja")
	public void user_mode_kerja(String mode_kerja) {
		if(mode_kerja=='select') {
			WebUI.selectOptionByValue(findTestObject('LogbookPage/select_mode_kerja'), 'WFO', true)
		}else if (mode_kerja=='not select') {
			WebUI.verifyElementVisible(findTestObject('LogbookPage/select_mode_kerja'))
		}
	}

	@When("User (.*) project")
	public void user_project(String project) {
		if (project=='input') {
			WebUI.setText(findTestObject('LogbookPage/input_project'), 'BUMN')
		}else if (project=='not input') {
			WebUI.setText(findTestObject('LogbookPage/input_project'), '')
		}
	}

	@When("User (.*) nama tim")
	public void user_nama_tim(String nama_tim) {
		if (nama_tim=='input') {
			WebUI.setText(findTestObject('LogbookPage/input_nama_tim'), 'Engineer Team')
		}else if (nama_tim=='not input') {
			WebUI.setText(findTestObject('LogbookPage/input_nama_tim'), '')
		}
	}

	@When("User (.*) use case")
	public void user_use_case(String use_case) {
		if (use_case=='input') {
			WebUI.setText(findTestObject('LogbookPage/input_use_case'), 'Test Use Case')
		}else if (use_case=='input') {
			WebUI.setText(findTestObject('LogbookPage/input_use_case'), '')
		}
	}

	@When("User (.*) kegiatan")
	public void user_kegiatan(String kegiatan) {
		if (kegiatan=='select') {
			WebUI.selectOptionByValue(findTestObject('LogbookPage/select_kegiatan'), 'Testing', true)
		}else if (kegiatan=='not input') {
			WebUI.verifyElementVisible(findTestObject('LogbookPage/select_kegiatan'))
		}
	}

	@When("User (.*) hasil")
	public void user_hasil(String hasil) {
		if (hasil=='input') {
			WebUI.setText(findTestObject('LogbookPage/input_hasil'), 'Memuaskan')
		}else if (hasil=='not input'){
			WebUI.setText(findTestObject('LogbookPage/input_hasil'), '')
		}
	}

	@When("User (.*) capaian")
	public void user_capaian(String capaian) {
		if (capaian=='input') {
			WebUI.setText(findTestObject('LogbookPage/input_capaian'), '100')
		}else if (capaian=='not input') {
			WebUI.setText(findTestObject('LogbookPage/input_capaian'), '')
		}
	}

	@When("User (.*) next step")
	public void user_next_step(String next_step) {
		if (next_step=='select') {
			WebUI.selectOptionByValue(findTestObject('LogbookPage/select_nextstep'), 'Continue', true)
		}else if (next_step=='not select') {
			WebUI.verifyElementVisible(findTestObject('LogbookPage/select_nextstep'))
		}
	}

	@When("User (.*) filename")
	public void user_filename(String filename) {
		if (filename=='input') {
			WebUI.setText(findTestObject('LogbookPage/input_file_name'), 'File Testing')
		}else if (filename=='not input') {
			WebUI.setText(findTestObject('LogbookPage/input_file_name'), '')
		}
	}

	@When("User (.*) detail pekerjaan")
	public void user_detail_pekerjaan(String detail_pekerjaan) {
		if (detail_pekerjaan=='input') {
			WebUI.setText(findTestObject('LogbookPage/txt_detail_pekerjaan'), 'Testing V1.1')
		}else if (detail_pekerjaan=='not input') {
			WebUI.setText(findTestObject('LogbookPage/txt_detail_pekerjaan'), '')
		}
	}

	@When("User (.*) waktu kerja")
	public void user_waktu_kerja(String waktu_kerja) {
		if (waktu_kerja=='select') {
			WebUI.selectOptionByValue(findTestObject('LogbookPage/select_wktkerja'), 'Jam Normal', true)
		}else if (waktu_kerja=='not select') {
			WebUI.verifyElementVisible(findTestObject('LogbookPage/select_wktkerja'))
		}
	}

	@When("User click submit button")
	public void user_click_submit_button() {
		WebUI.click(findTestObject('LogbookPage/btn_Submit'))
	}

	@Then("User (.*) add logbook")
	public void user_add_logbook(String result) {
		if (result=='successfully') {
			WebUI.verifyTextPresent('Logbook berhasil ditambahkan', false)
		}else if (result=='failed') {
			WebUI.verifyElementVisible(findTestObject('LogbookPage/btn_Submit'))
		}
	}
}
