@SignOut
Feature: Sign Out

  Background: User do login process
    Given User go to login page
    When User input username field with "TestAccountKaryawan1"
    And User input password field with "P@sswordK1"
    And User click Login button
    Then User successfully login

   Scenario: User want to sign out from web
   When User click icon profile
   And User click sign in button
   Then User succesfully sign out