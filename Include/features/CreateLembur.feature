@CreateLembur
Feature: Create Lembur

  Background: User do login process
    Given User go to login page
    When User input username field with "TestAccountKaryawan1"
    And User input password field with "P@sswordK1"
    And User click Login button
    Then User successfully login

  Scenario Outline: User want to create lembur <condition>
    When User click cuti lembur menu button
    And User click pengajuan lembur
    And User click ajukan lembur
    And User <tgl_lembur> tanggal lembur
    And User <jam_mulai> jam mulai lembur
    And User <jam_selesai> jam selesai lembur
    And User <jenis_lembur> jenis lembur
    And User <nama_pic> nama pic
    And User <ket_lembur> keterangan lembur
    And User <signature> digital signature
    And User click submit
    Then User <result> create lembur

    Examples: 
      | condition    | tgl_lembur | jam_mulai  | jam_selesai | jenis_lembur | nama_pic   | ket_lembur | signature  | result       |
      | successfully | select     | select     | select      | select       | select     | input      | create     | successfully |
      | failed       | not select | not select | not select  | not select   | not select | not input  | not create | failed       |
