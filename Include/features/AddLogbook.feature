@AddLogbook
Feature: Add Logbook

  Background: User do login process
    Given User go to login page
    When User input username field with "TestAccountKaryawan1"
    And User input password field with "P@sswordK1"
    And User click Login button
    Then User successfully login

  Scenario Outline: User want to add logbook <condition>
    When User click tambah logbook button
    And User <tanggal> tanggal
    And User <jam_mulai> jam mulai
    And User <jam_selesai> jam selesai
    And User <mode_kerja> mode kerja
    And User <project> project
    And User <nama_tim> nama tim
    And User <use_case> use case
    And User <kegiatan> kegiatan
    And User <hasil> hasil
    And User <capaian> capaian
    And User <next_step> next step
    And User <filename> filename
    And User <detail_pekerjaan> detail pekerjaan
    And User <waktu_kerja> waktu kerja
    And User click submit button
    Then User <result> add logbook

    
    Examples: 
      | condition    | tanggal    | jam_mulai  | jam_selesai | mode_kerja | project   | nama_tim  | use_case  | kegiatan   | hasil     | capaian   | next_step  | filename  | detail_pekerjaan | waktu_kerja | result       |
      | successfully | select     | select     | select      | select     | input     | input     | input     | select     | input     | input     | select     | input     | input            | select      | successfully |
      | failed       | not select | not select | not select  | not select | not input | not input | not input | not select | not input | not input | not select | not input | not input        | not select  |              |
