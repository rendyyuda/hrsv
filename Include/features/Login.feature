@Login
Feature: Login

  Scenario Outline: User want to login with <condition>
    Given User go to login page
    When User input username field with <username> username
    And User input password field with <password> password
    And User click Login button
    Then User <result> login

    Examples: 
      | case_id | condition          | username | password | result       |
      | A01     | correct credential | valid    | valid    | successfully |
      | A02     | invalid password   | valid    | wrong    | failed       |