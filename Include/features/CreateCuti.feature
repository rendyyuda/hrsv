@CreateCuti
Feature: Create Cuti

  Background: User do login process
    Given User go to login page
    When User input username field with "TestAccountKaryawan1"
    And User input password field with "P@sswordK1"
    And User click Login button
    Then User successfully login

  Scenario Outline: User want to create Cuti
    When User click cuti lembur menu
    And User click pengajuan cuti
    And User click ajukan cuti
    And User select date
    And User <cuti> jenis cuti
    And User select nama pic
    And User input keterangan cuti
    And User create signature
    And User click submit btn
    Then User <result> create cuti
    
   Examples:
   |	 cuti		|	  result	 |
   |	select	|successfully|
   |not select|		failed	 |