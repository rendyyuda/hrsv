<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_--Select--WFOWFH</name>
   <tag></tag>
   <elementGuidId>29f4c569-cd01-4371-9fde-e9c102ca8056</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>select[name=&quot;mode_kerja&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@name='mode_kerja']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>c030ee67-0d55-4630-af77-68414e86a23d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>62b72ab5-653e-4ea1-a11b-621e1c53ecb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>-Select-</value>
      <webElementGuid>55d56093-6daa-4b51-aa51-87c546cf4cbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>mode_kerja</value>
      <webElementGuid>4243c082-89ad-4045-a146-9b52d19214fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>--Select--WFOWFH</value>
      <webElementGuid>8215a2fa-13b1-4323-bb60-a768a7687820</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;c-app c-default-layout&quot;]/div[@class=&quot;c-wrapper&quot;]/div[@class=&quot;c-body&quot;]/main[@class=&quot;c-main&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;fade show&quot;]/div[@class=&quot;container-mains&quot;]/div[4]/div[@class=&quot;modal overflow-auto fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered&quot;]/div[@class=&quot;modal-content&quot;]/form[1]/div[@class=&quot;mb-4 card-body&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;row form-group my-0 mx-2&quot;]/div[@class=&quot;col-8&quot;]/div[@class=&quot;form-group&quot;]/select[@class=&quot;form-control&quot;]</value>
      <webElementGuid>d98079d9-cee7-43a4-ac0c-b2c7071c4e02</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@name='mode_kerja']</value>
      <webElementGuid>feac1f57-62b3-47f5-8461-e027ab9769f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[3]/div/main/div/div/div/div[4]/div/div/div/form/div/div[2]/div[4]/div[2]/div/select</value>
      <webElementGuid>fbabe726-1ae0-40df-b675-daaf459b6774</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mode Kerja'])[1]/following::select[1]</value>
      <webElementGuid>9c45969b-2495-4a35-9314-9c9a1f657c24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jam Selesai'])[1]/following::select[1]</value>
      <webElementGuid>d9c81d94-6402-48ee-8ca3-ddc0c30f0bcb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Project'])[1]/preceding::select[1]</value>
      <webElementGuid>904b624d-9d11-4c22-91a5-a39d59edd27d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama Tim'])[1]/preceding::select[1]</value>
      <webElementGuid>1493f221-d954-492d-b0ac-982c2ed0351d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/select</value>
      <webElementGuid>efc8ed19-2926-45e8-91b8-7316fec7982e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@placeholder = '-Select-' and @name = 'mode_kerja' and (text() = '--Select--WFOWFH' or . = '--Select--WFOWFH')]</value>
      <webElementGuid>006b7e6f-b62b-4375-ac87-4216e3a3639a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
