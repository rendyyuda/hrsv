<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_ajukan_cuti</name>
   <tag></tag>
   <elementGuidId>8c1a1fc3-ec56-4bc6-9a25-025d4f73ad34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[3]/div/main/div/div/div/div[2]/div/div[2]/a/button/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.button-gradient-blue.btn > div > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>f3788f0e-2e98-4253-b7bb-264682226e67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ajukan Cuti</value>
      <webElementGuid>0b96bd96-301c-4c39-8b88-7561d6e0bd38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;c-app c-default-layout&quot;]/div[@class=&quot;c-wrapper&quot;]/div[@class=&quot;c-body&quot;]/main[@class=&quot;c-main&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;fade show&quot;]/div[@class=&quot;container-mains&quot;]/div[@class=&quot;mx-4&quot;]/div[@class=&quot;align-items d-flex justify-content-between mb-2&quot;]/div[2]/a[@class=&quot;nav-link&quot;]/button[@class=&quot;button-gradient-blue btn&quot;]/div[1]/span[1]</value>
      <webElementGuid>e3aa6193-4b25-481e-98ff-5686cd627745</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[3]/div/main/div/div/div/div[2]/div/div[2]/a/button/div/span</value>
      <webElementGuid>cdbb53ee-18c1-4a3f-b142-ea2779d9b448</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Riwayat Pengajuan Cuti'])[1]/following::span[1]</value>
      <webElementGuid>1e65e156-2251-49dd-9d96-4b945e40f402</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tidak Disetujui'])[1]/following::span[2]</value>
      <webElementGuid>82342bfd-f7eb-434e-ba3e-a16401d6e197</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/preceding::span[1]</value>
      <webElementGuid>fbbb5e1d-238e-4a2d-969c-396cd88aeb5d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tanggal Cuti'])[1]/preceding::span[1]</value>
      <webElementGuid>a4713b95-8140-4707-a9d4-ec9f53d12489</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Ajukan Cuti']/parent::*</value>
      <webElementGuid>7f2008de-0ce2-4b99-b3fc-e350e299ef84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button/div/span</value>
      <webElementGuid>d55100e0-71f5-4e6d-9e62-9ec1e3e8ba40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Ajukan Cuti' or . = 'Ajukan Cuti')]</value>
      <webElementGuid>c8eca0e3-7a4b-43f3-8eb5-3939c0af18d0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
