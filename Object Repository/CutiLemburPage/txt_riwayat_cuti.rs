<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_riwayat_cuti</name>
   <tag></tag>
   <elementGuidId>0d23f737-b9fe-4666-9a67-465af6398272</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[3]/div/main/div/div/div/div[2]/div/div/h5</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div > h5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>15e7f274-93fb-437b-9e3f-2558487b944d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Riwayat Pengajuan Cuti</value>
      <webElementGuid>baddf390-a5b8-4cac-8883-9f6846e8b359</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;c-app c-default-layout&quot;]/div[@class=&quot;c-wrapper&quot;]/div[@class=&quot;c-body&quot;]/main[@class=&quot;c-main&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;fade show&quot;]/div[@class=&quot;container-mains&quot;]/div[@class=&quot;mx-4&quot;]/div[@class=&quot;align-items d-flex justify-content-between mb-2&quot;]/div[1]/h5[1]</value>
      <webElementGuid>8b20a947-6a26-48bd-8f90-e72a0ea76b38</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[3]/div/main/div/div/div/div[2]/div/div/h5</value>
      <webElementGuid>41df7a11-775b-4bf0-8bdf-100295efed23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tidak Disetujui'])[1]/following::h5[1]</value>
      <webElementGuid>d2d25aa9-1852-4d35-b685-5e86477c0a89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Menunggu Disetujui'])[1]/following::h5[1]</value>
      <webElementGuid>819ae04d-7b0d-4cf3-a488-ba37bc545d6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ajukan Cuti'])[1]/preceding::h5[1]</value>
      <webElementGuid>b5b72f0d-2716-4dae-8db0-27ca8f300df4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/preceding::h5[1]</value>
      <webElementGuid>dfe4f66f-2d1a-41f3-803c-6677ac087223</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Riwayat Pengajuan Cuti']/parent::*</value>
      <webElementGuid>d911f3c4-71d1-4626-a42d-4a9b3e6874a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/h5</value>
      <webElementGuid>c403a679-5c60-4c73-a36a-f4e49bdbb3c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Riwayat Pengajuan Cuti' or . = 'Riwayat Pengajuan Cuti')]</value>
      <webElementGuid>a7dc8d94-4848-4410-80b1-303f9bc14f54</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
