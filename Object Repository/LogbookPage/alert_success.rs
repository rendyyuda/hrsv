<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alert_success</name>
   <tag></tag>
   <elementGuidId>d011398a-0067-4052-a2fc-c9386fc864be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.swal-modal</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[2]/div/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d984792d-2451-47e6-b718-c0326e7bdc8b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>swal-modal</value>
      <webElementGuid>d452a010-7de2-47bb-b0dd-a49e70155473</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>dialog</value>
      <webElementGuid>f80742d5-22e1-4acc-a3cc-c3e87445aec0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-modal</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>1c4d9f3d-6fc3-4faa-b546-b3547a3455fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    

    
    
  SuccessLogbook berhasil ditambahkan

    OK

    
      
      
      
    

  </value>
      <webElementGuid>85b0c1fa-6e79-43f6-a405-6b997793f10a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;swal-overlay swal-overlay--show-modal&quot;]/div[@class=&quot;swal-modal&quot;]</value>
      <webElementGuid>b810f71d-1a07-4d29-9af2-09a5b628257a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[3]</value>
      <webElementGuid>ad98ab9e-c961-432e-9b1b-bc416efe9d1a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[2]/following::div[3]</value>
      <webElementGuid>18200a7d-6cc4-4f51-9537-c98969dc2983</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div</value>
      <webElementGuid>6465e167-0ed2-4930-8251-01a044ec5c2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
    

    
    
  SuccessLogbook berhasil ditambahkan

    OK

    
      
      
      
    

  ' or . = '
    
    

    
    
  SuccessLogbook berhasil ditambahkan

    OK

    
      
      
      
    

  ')]</value>
      <webElementGuid>0e48afe8-cb85-406a-91f0-707a29627cab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
